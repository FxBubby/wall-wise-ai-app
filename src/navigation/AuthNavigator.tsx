import React from 'react';
import {RouteProp} from '@react-navigation/native';
import {
    createNativeStackNavigator,
    NativeStackNavigationProp,
} from '@react-navigation/native-stack';
import {AppRoute} from '../constants/AppRoute';
import LoginScreen from "../screens/Login";


export type AuthNavigatorParamList = {
    [AppRoute.LOGIN_SCREEN]: undefined;
};

export interface AuthNavigatorProps<
    Screen extends keyof AuthNavigatorParamList,
> {
    navigation: NativeStackNavigationProp<AuthNavigatorParamList, Screen>;
    route: RouteProp<AuthNavigatorParamList, Screen>;
}

export type LoginScreenProps = AuthNavigatorProps<AppRoute.LOGIN_SCREEN>;

const Stack = createNativeStackNavigator<AuthNavigatorParamList>();

const AuthNavigator: React.FC = () => {
    return (
        <Stack.Navigator
            initialRouteName={AppRoute.LOGIN_SCREEN}
            screenOptions={{
                headerShown: false,
                animation: 'slide_from_right',
                gestureEnabled: false,
            }}>
            <Stack.Screen name={AppRoute.LOGIN_SCREEN} component={LoginScreen} />
        </Stack.Navigator>
    );
};

export default AuthNavigator;
