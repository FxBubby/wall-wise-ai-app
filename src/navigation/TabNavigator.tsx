import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {Text} from 'react-native';
// import ChatScreen from './ChatScreen';
// import VoiceScreen from './VoiceScreen';
// import ProfileScreen from './ProfileScreen';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Chat" component={() => <Text>chat</Text>} />
      <Tab.Screen name="Voice" component={() => <Text>Voice</Text>} />
      <Tab.Screen name="Profile" component={() => <Text>Profile</Text>} />
    </Tab.Navigator>
  );
};

export default TabNavigator;
