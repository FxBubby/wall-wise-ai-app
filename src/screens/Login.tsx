import React, {useContext, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import {AuthContext, User} from '../context/AuthContext';
import {Colors} from '../constants/Colors';
const LoginScreen = () => {
  const backgroundImage = require('../../assets/images/background.png');
  const {login} = useContext(AuthContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = () => {
    // Perform login logic, e.g., call an API
    const userData: User = {email, password, firstTIme: false};
    console.log('Logging in:', userData);
    login(userData);
  };

  return (
    <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
      {/* Your other components */}
    </ImageBackground>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
});
